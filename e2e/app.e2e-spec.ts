import { PopAppPage } from './app.po';

describe('pop-app App', () => {
  let page: PopAppPage;

  beforeEach(() => {
    page = new PopAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
